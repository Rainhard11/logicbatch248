﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal01
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukan n : ");;
            int panjangAngka = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan n2 : "); ;
            int rumusDeret = int.Parse(Console.ReadLine());
            int[,] angkaArray = new int[2,panjangAngka];

            Console.WriteLine();

            //Value Creation
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < panjangAngka; j++)
                {
                    if (i == 0)
                    {
                        angkaArray[i, j] = j;
                    }
                    else
                    {
                        angkaArray[i,j] = Convert.ToInt32(Math.Pow(rumusDeret, j));
                    }
                }
            }

            Utility.PrintArray2D(angkaArray);

        }
    }
}
