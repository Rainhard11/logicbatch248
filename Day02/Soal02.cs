﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal02
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukan n : "); ;
            int panjangAngka = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan n2 : "); ;
            int rumusDeret = int.Parse(Console.ReadLine());
            int[,] angkaArray = new int[2, panjangAngka];
            int deretBantuDua = 1;

            Console.WriteLine();

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < panjangAngka; j++)
                {
                    if (i == 0)
                    {
                        angkaArray[i, j] = j;
                       
                    }
                    else if (i==1)
                    {
                        deretBantuDua++;
                        if (deretBantuDua % 3 == 0)
                        {
                            angkaArray[i, j] = Convert.ToInt32(Math.Pow(rumusDeret, j));
                            angkaArray[i, j] *= -1;

                        }
                        else
                        {
                            angkaArray[i, j] = Convert.ToInt32(Math.Pow(rumusDeret, j));
                        }
                    }
                }
            }

            Utility.PrintArray2D(angkaArray);
        }
    }
}
