﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal03
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukan n : "); ;
            int panjangAngka = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan n2 : "); ;
            int rumusDeret = int.Parse(Console.ReadLine());
            int[,] angkaArray = new int[2, panjangAngka];
            int angka = 1;

            Console.WriteLine();

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < panjangAngka; j++)
                {
                    if (i == 0)
                    {
                        angkaArray[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        if (panjangAngka % 2 == 1)
                        {
                            if (j <= panjangAngka / 2)
                            {
                                angkaArray[i, j] = rumusDeret;
                                angkaArray[i, panjangAngka - 1 - j] = rumusDeret;
                                rumusDeret = rumusDeret * 2;
                            }
                        }
                        else
                        {
                            if (j < panjangAngka / 2)
                            {
                                angkaArray[i, j] = rumusDeret;
                                angkaArray[i, panjangAngka - 1 - j] = rumusDeret;
                                rumusDeret = rumusDeret * 2;
                            }

                        }
                    }
                }
            }
            
            Utility.PrintArray2D(angkaArray);
        }
    }
}

