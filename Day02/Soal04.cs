﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal04
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukan n : "); ;
            int panjangAngka = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan n2 : "); ;
            int rumusDeret = int.Parse(Console.ReadLine());
            int[,] angkaArray = new int[2, panjangAngka];
            int deretBantu = 0;
            int deretKedua = 0;
            int barisKesatu = 0;

            Console.WriteLine();

            //Value Creation
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < panjangAngka; j++)
                {
                    if (i == 0)
                    {
                        angkaArray[i, j] = j;
                    }
                    else
                    {
                        if (deretBantu%2==0)
                        {
                            barisKesatu++;
                            angkaArray[i, j] = barisKesatu;
                        }
                        else
                        {
                            deretKedua++;
                            angkaArray[i, j] = deretKedua*rumusDeret;
                        }
                        deretBantu++;
                    }
                }
            }

            Utility.PrintArray2D(angkaArray);

        }
    }
}
