﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal05
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukan n : ");
            int panjangAngka = int.Parse(Console.ReadLine());
            int[,] angkaArray = new int[3, panjangAngka];
            int angkaDeret = 0;

            Console.WriteLine();

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < panjangAngka; j++)
                {
                    angkaArray[i, j] = angkaDeret;
                    angkaDeret++;
                }
            }

            Utility.PrintArray2D(angkaArray);
        }
    }
}
