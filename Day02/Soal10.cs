﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal10
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Masukan n : ");
            int panjangAngka = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan n2 : "); ;
            int rumusDeret = int.Parse(Console.ReadLine());

            int[,] angkaArray = new int[3, panjangAngka];

            Console.WriteLine();

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < panjangAngka; j++)
                {
                    if (i == 0)
                    {
                        angkaArray[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        angkaArray[i, j] = rumusDeret * j;
                    }
                    else
                    {
                        angkaArray[i, j] = (rumusDeret+1) * j;
                    }
                }
            }

            Utility.PrintArray2D(angkaArray);
        }
    }
}
