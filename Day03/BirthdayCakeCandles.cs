﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class BirthdayCakeCandles
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan deret angka: ");
            string numbers = Console.ReadLine();
            int[] numbersArray = Utility.ConvertStringToIntArry(numbers);
            int lilinTertinggi = numbersArray.Max();
            int lilin = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] == lilinTertinggi)
                {
                    lilin += 1;
                }
            }

            Console.WriteLine("\n" + lilin);
            Console.ReadKey();
        }
    }
}
