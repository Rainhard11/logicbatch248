﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class ComparetheTriplets
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan Score Alice: ");
            string scoreAlice = Console.ReadLine();

            Console.WriteLine("Masukan Score Bob: ");
            string scoreBob = Console.ReadLine();

            int[] aliceArray = Utility.ConvertStringToIntArry(scoreAlice);

            int[] bobsArray = Utility.ConvertStringToIntArry(scoreBob);

            int alice = 0;
            int bob = 0;

            if (aliceArray.Length != bobsArray.Length)
            {
                Console.WriteLine("Input yang anda masukan Salah");
            }

            else
            {
                for (int i = 0; i < aliceArray.Length; i++)
                {
                    if (aliceArray[i] > bobsArray[i])
                    {
                        alice += 1;
                    }
                    else if (aliceArray[i] < bobsArray[i])
                    {
                        bob += 1;
                    }
                }

                Console.WriteLine("Alice : " + alice + " " + "Bob : " + bob);

            }

        }
    }
}
