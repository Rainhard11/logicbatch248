﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifference
    {
        public static void Resolve()
        {
            // Only for input
            Console.WriteLine("Input the length of the matrix: ");
            int length = int.Parse(Console.ReadLine());

            int[,] array2D = new int[length, length];
            int diagonal1 = 0;
            int diagonal2 = 0;

            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter the " + (i + 1) + " set of numbers");
                string numbers = Console.ReadLine();

                int[] array = Utility.ConvertStringToIntArry(numbers);
                if (array.Length != length)
                {
                    Console.WriteLine("worng numbers, try again");
                    break;
                }
                else
                {
                    for (int j = 0; j < length; j++)
                    {
                        array2D[i, j] = array[j];
                    }
                }
            }
            Console.WriteLine();

            // Cetak Diagonal Pertama
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i == j )
                    {
                        Console.Write(array2D[i, j] + "\t");
                        diagonal1 = diagonal1 + array2D[i, j];
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine("\n");
            Console.WriteLine("Diagonal ke 1 = " + diagonal1);
            Console.WriteLine("\n");
            // Cetak Diagonal Kedua
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i + j == array2D.GetLength(1) - 1)
                    {
                        Console.Write(array2D[i, j] + "\t");
                        diagonal2 = diagonal2 + array2D[i, j];
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine("\n");
            Console.WriteLine("Diagonal ke 2 = " + diagonal2);
            Console.WriteLine("Absolute : " + Math.Abs(diagonal1-diagonal2)); 

            Console.ReadKey();
        }
    }
}
