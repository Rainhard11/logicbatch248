﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Mini_MaxSum
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan deret angka: ");
            string numbers = Console.ReadLine();
            int[] numbersArray = Utility.ConvertStringToIntArry(numbers);

            int sumTerendah = 0;
            int sumTertinggi = 0;


            Array.Sort(numbersArray);
            Console.WriteLine();
            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (i < (numbersArray.Length -1))
                {
                    Console.Write(numbersArray[i] + " ");
                    sumTerendah += numbersArray[i];
                }
                
            }

            Console.WriteLine();

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (i==0)
                {
                    numbersArray[i] = 0;
                }
                else
                {
                    Console.Write(numbersArray[i] + " ");
                    sumTertinggi += numbersArray[i];
                }

            }
            Console.WriteLine();
            Console.WriteLine("Hasil terrendah: " + sumTerendah);
            Console.WriteLine("Hasil tertinggi: " + sumTertinggi);



            Console.ReadKey();
        }
    }
}
