﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class PlusMinus
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan deret angka: ");
            string numbers = Console.ReadLine();
            int[] numbersArray = Utility.ConvertStringToIntArry(numbers);
            double negative = 0;
            double zero = 0;
            double positive = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] < 0)
                {
                    negative += 1;
                }

                else if (numbersArray[i] > 0)
                {
                    positive += 1;
                }
                else
                {
                    zero += 1;
                }
            }

            Console.WriteLine();
            Console.WriteLine("positive : " + positive / numbersArray.Length);
            Console.WriteLine("negative : " + negative / numbersArray.Length);
            Console.WriteLine("zero : " + zero/numbersArray.Length);

            Console.ReadKey();
        }
    }
}
