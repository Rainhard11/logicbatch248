﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Solve Me First");
                        Console.WriteLine();
                        SolveMeFirst.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("02. Time Conversion");
                        Console.WriteLine();
                        TimeConversion.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("03. Simple Array Sum");
                        Console.WriteLine();
                        SimpleArraySum.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("04. Diagonal Difference");
                        Console.WriteLine();
                        DiagonalDifference.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("05. Plus Minus");
                        Console.WriteLine();
                        PlusMinus.Resolve();
                        break;

                    case 6:
                        
                        Console.WriteLine("06.Staircase");
                        Console.WriteLine();
                        Staircase.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("07. Mini-Max Sum");
                        Console.WriteLine();
                        Mini_MaxSum.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("08. Birthday Cake Candles");
                        Console.WriteLine();
                        BirthdayCakeCandles.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("09. A Very Big Sum");
                        Console.WriteLine();
                        AVeryBigSum.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("10. Compare the Triplets");
                        Console.WriteLine();
                        ComparetheTriplets.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
