﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class SimpleArraySum
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan deret angka: ");
            string numbers = Console.ReadLine();
            int[] numbersArray = Utility.ConvertStringToIntArry(numbers);
            int total = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }
            Console.WriteLine("Totalnya adalah: " + total);

            Console.ReadKey();

        }
    }
}
