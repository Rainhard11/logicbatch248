﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class SolveMeFirst
    {
        public static void Resolve()
        {
            Console.WriteLine("Nilai A: ");
            int inputA =int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.WriteLine("Nilai B: ");
            int inputB = int.Parse(Console.ReadLine());

            Console.WriteLine("Hasil: " + Utility.Sum (inputA, inputB));
            Console.ReadKey();
            
        }
    }
}

