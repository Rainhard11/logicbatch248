﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class TimeConversion
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan Waktu (hh:mm:ss AM/PM): ");
            string dateTime = Console.ReadLine();

            if (!dateTime.Contains("AM") && !dateTime.Contains("PM"))
            {
                Console.WriteLine("Format yang anda masukan Salah");
            }
            else
            {
                string[] Split = dateTime.Split(':' , ' ');
                string jam = Split[0];
                string menit = Split[1];
                string detik = Split[2];
                string timeFormat = Split[3];

                //string[] Split2 = detikFormat.Split(' ');
                //string detik = Split2[0];
                //string timeFormat = Split2[1];


                if (int.Parse(jam) > 12 || int.Parse(menit) > 60 || int.Parse(detik) > 60)
                {
                    Console.WriteLine("Format yang anda masukan Salah");
                }

                else
                {
                    if (timeFormat.ToLower() == "pm")
                    {
                        if (int.Parse(jam) == 12)
                        {
                            jam = jam;
                        }
                        else
                        {
                            int jamPM = int.Parse(jam) + 12;
                            jam = jamPM.ToString();
                        }
                    }

                    else if (timeFormat.ToLower() == "am")
                    {
                        if (int.Parse(jam) == 12)

                            jam = "00";
                    }
                    Console.WriteLine("Time ConversionPage (24h) : " + jam + ":" + menit + ":" + detik);
                }
            }
            Console.ReadKey();
        }

    }
}

