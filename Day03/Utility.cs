﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Utility
    {
        public static int Sum(int inputA, int inputB)
        {
            int total = total = inputA + inputB;
            return total;
        }

        public static int[] ConvertStringToIntArry(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numberArray = new int[stringNumbersArray.Length];

            // Convert to int
            for (int i = 0; i < numberArray.Length; i++)
            {
                numberArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numberArray;
        }

        public static long[] ConvertStringToLongArry(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            long[] numberArray = new long[stringNumbersArray.Length];

            // Convert to long
            long[] numberArray1 = numberArray;
            for (int i = 0; i < numberArray1.Length; i++)
            {
                numberArray1[i] = Convert.ToInt64(stringNumbersArray[i]);
            }
            return numberArray1;
        }

        public static void PrintArray2D(int[,] array2D)
        {
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    Console.Write(array2D[i, j] + "\t");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
