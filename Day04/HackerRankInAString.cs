﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class HackerRankInAString
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan Total Inputan :");
            int totalInputan = int.Parse(Console.ReadLine());
            string[] inputanArray = new string[totalInputan];


            for (int i = 0; i < totalInputan; i++)
            {
                Console.WriteLine("Masukan Inputan Ke-" + (i + 1));
                inputanArray[i] = Console.ReadLine().ToLower();

            }

            Console.WriteLine("Masukan Kata yang ingin dicari: ");
            string pengecekan = Console.ReadLine().ToLower(); ;
            char[] pengecekanArray = pengecekan.ToCharArray();

            for (int i = 0; i < inputanArray.Length; i++)
            {
                char[] array = inputanArray[i].ToCharArray();
                int alamatArrayPengecekan = 0;

                for (int j = 0; j < array.Length; j++)
                {
                    if (alamatArrayPengecekan < pengecekan.Length)
                    {
                        if (array[j] == pengecekanArray[alamatArrayPengecekan])
                        {
                            alamatArrayPengecekan++;
                        }
                    }
                }

                if (alamatArrayPengecekan != pengecekanArray.Length)
                {
                    Console.WriteLine("Jawaban Kalimat Ke-" + (i + 1) + " Adalah NO");
                }
                else
                {
                    Console.WriteLine("Jawaban Kalimat Ke-" + (i + 1) + " Adalah YES");
                }
            }


            Console.ReadKey();


        }
    }
}
