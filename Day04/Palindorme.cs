﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Palindorme
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan Kalimat: ");
            string kalimat = Console.ReadLine();
            string KaliamtAwal = kalimat;
            Console.WriteLine();

            char[] charArray = kalimat.ToCharArray();
            Array.Reverse(charArray);
            string kalimatTerbalik = new string(charArray);
            string hasil = "";

            Console.WriteLine("Dibalik menjadi :" + kalimatTerbalik);

            if (KaliamtAwal == kalimatTerbalik)
            {
                hasil = "IYA";
            }
            else
            {
                hasil = "TIDAK";
            }

            Console.WriteLine();
            Console.WriteLine("Kalimat Palindrome ? " + hasil);
        }
    }
}
