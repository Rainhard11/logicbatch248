﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Pangrams
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan kalimat: ");
            string pangrams = Console.ReadLine().ToLower();

            char[] pangramsChar = pangrams.ToCharArray();

            int cek = 0;

            string pengecekan = "abcdefghijklmnopqrstuvwxyz ";
            char[] pengecekanArray = pengecekan.ToCharArray();


            for (int i = 0; i < pengecekanArray.Length; i++)
            {
                for (int j = 0; j < pangramsChar.Length; j++)
                {
                    if (pengecekanArray[i] == pangramsChar[j])
                    {
                        cek++;
                        Console.Write(pengecekanArray[i] + " ");
                        //break;
                    }
                }
            }
            if (cek != 27)
            {
                Console.WriteLine("Bukan Panagram");
            }
            else
            {
                Console.WriteLine("Ini Panagram");
            }
        }
    }
}


