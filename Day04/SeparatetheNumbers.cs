﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class SeparatetheNumbers
    {
        public static void Resolve()
        {
            //Console.WriteLine("Masukan Total Inputan :");
            //int totalInputan = int.Parse(Console.ReadLine());
            //string[] inputanArray = new string[totalInputan];

            //for (int i = 0; i < totalInputan; i++)
            //{
            //    Console.WriteLine("Masukan Inputan Ke-" + (i + 1));
            //    inputanArray[i] = Console.ReadLine();
            //}

            Console.WriteLine("Masukan Inputan");
            string deret = Console.ReadLine();

            string subString = "";
            bool isValid = false;

            for (int i = 0; i < deret.Length / 2; i++)
            {
                int number = int.Parse(deret.Substring(0, i + 1));
                string result = "";
                int index = 0;

                while (result.Length < deret.Length)
                {
                    result += (number + index).ToString();
                    index++;
                }

                if (result.Equals(deret))
                {
                    isValid = true;
                    break;
                }
            }

            if (isValid)
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");

            }

        }
    }
}
