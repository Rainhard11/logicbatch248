﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class StrongPassword
    {
        public static void Resolve()
        {
            Console.WriteLine("Buat Password anda ");
            string password = Console.ReadLine();
            Console.WriteLine();

            char[] passwordChar = password.ToCharArray();
            int verivikasiNumber = 0;
            int verivikasiKarakterKecil = 0;
            int verivikasiKarakterBesar = 0;
            int verivikasiSimbol = 0;
            int kurangPanjangPassword = 6;

            for (int i = 0; i < passwordChar.Length; i++)
            {
                if (char.IsNumber(passwordChar[i]))
                {
                    verivikasiNumber = +1;
                    kurangPanjangPassword--;
                }
                if (char.IsLower(passwordChar[i]))
                {
                    verivikasiKarakterKecil += 1;
                    kurangPanjangPassword--;

                }
                if (char.IsUpper(passwordChar[i]))
                {
                    verivikasiKarakterBesar += 1;
                    kurangPanjangPassword--;

                }
                if (!char.IsLetterOrDigit(passwordChar[i]))
                {
                    verivikasiSimbol += 1;
                    kurangPanjangPassword--;
                }
            }

            if (kurangPanjangPassword<0)
            {
                kurangPanjangPassword = 0;
            }

            string number = "";
            string karakterKecil = "";
            string karakterBesar = "";
            string simbol = "";
            int kurangKarakter = 0;

            if (verivikasiNumber < 1 || verivikasiKarakterBesar < 1 || verivikasiKarakterKecil < 1 || verivikasiSimbol < 1)
            {
                if (verivikasiNumber < 1)
                {
                    number = ", digit numbers";
                    kurangKarakter++;
                }
                if (verivikasiKarakterBesar < 1)
                {
                    karakterBesar = ", karakter Besar";
                    kurangKarakter++;
                }
                if (verivikasiKarakterKecil < 1)
                {
                    karakterKecil = ", karakter kecil";
                    kurangKarakter++;
                }
                if (verivikasiSimbol < 1)
                {
                    simbol = ", spesial karakter";
                    kurangKarakter++;
                }

                int selisih = kurangPanjangPassword - kurangKarakter;

                if (selisih<0)
                {
                    selisih = 0;
                }

                Console.WriteLine("Password anda masih lemah silahkan tambah" + number + karakterBesar + karakterKecil + simbol);
                Console.WriteLine();
                Console.WriteLine("kurang " + (kurangKarakter + selisih) + " Karakter lagi...");

            }
        }
    }
}