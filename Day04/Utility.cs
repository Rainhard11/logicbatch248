﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Utility
    {
        public static int[] ConvertStringToIntArry(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numberArray = new int[stringNumbersArray.Length];

            // Convert to int
            for (int i = 0; i < numberArray.Length; i++)
            {
                numberArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numberArray;
        }

    }
}
