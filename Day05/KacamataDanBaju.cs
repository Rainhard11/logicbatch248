﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class KacamataDanBaju
    {
        public static void Resolve()
        {
            Console.WriteLine("Jumlah uang ");
            int uang = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan harga kacamata ");
            string numbers1 = Console.ReadLine();

            Console.WriteLine("Masukan harga baju ");
            string numbers2 = Console.ReadLine();

            int[] numbersArray1 = Utility.ConvertStringToIntArry(numbers1);
            int total1 = 0;

            int[] numbersArray2 = Utility.ConvertStringToIntArry(numbers2);
            int total2 = 0;

            for (int i = 0; i < numbersArray1.Length; i++)
            {
                total1 += numbersArray1[i];
            }

            for (int i = 0; i < numbersArray2.Length; i++)
            {
                total2 += numbersArray2[i];
            }

            Console.WriteLine("Total harga kacamata " + total1);
            Console.WriteLine("Total harga baju " + total2);
            Console.WriteLine("Jumlah yang harus dibayar " + Utility.SumTotal(total1, total2));

            int total3 = uang - Utility.SumTotal(total1, total2);

            if (uang < Utility.SumTotal(total1, total2))
            {
                Console.WriteLine("Dana tidak mencukupi ");
            }
            else
            {
                Console.WriteLine("Sisa uang  " + total3);
            }
        }
    }
}
