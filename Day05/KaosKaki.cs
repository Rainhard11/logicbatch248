﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class KaosKaki
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan deretan angka: ");
            string deretAngka = Console.ReadLine();

            if (!deretAngka.Contains(' ') || deretAngka.Contains('-'))
            {
                Console.WriteLine("Inputan yang anda masukan salah, tidak ada pasangan");
            }
            else
            {
                //Merubah ke int
                int[] angkaArray = Utility.ConvertStringToIntArry(deretAngka);

                int sepasang = 0;

                //Metode untuk sort
                Array.Sort(angkaArray);
                    
                //Mencari Pasangan
                for (int i = 0; i < angkaArray.Length - 1; i++)
                {
                    if (angkaArray[i] == angkaArray[i + 1])
                    {
                        sepasang++;
                        i++;
                    }
                }

                Console.WriteLine("Total Pasangan :" + sepasang);
            }
            
        }
    }
}
