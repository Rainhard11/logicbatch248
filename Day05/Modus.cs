﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Modus
    {
        public static void Resolve()
        {
            //inputan = 1 2 2 3 3 4 0 
            //output = 2 dan 3
            
            Console.WriteLine("Masukan deret angka: ");
            string deretAngka = Console.ReadLine();
            int[] intArray = Utility.ConvertStringToIntArry(deretAngka);

            Array.Sort(intArray);

            int count = 1;
            int maksimum = 0;
            int temp = 0;

            string result = "";

            for (int i = 0; i < intArray.Length - 1; i++)
            {
                if (intArray[i] == intArray[i + 1])
                {
                    count++;
                }
                else
                {
                    if (count > maksimum)
                    {
                        maksimum = count;

                        result = intArray[i].ToString() + " ";

                    }
                    else if (count == maksimum)
                    {
                        result += intArray[i].ToString() + " ";
                    }
                    count = 1;
                }

            }
            if (count > maksimum)
            {
                maksimum = count;

                result = intArray[intArray.Length - 1].ToString() + " ";

            }
            else if (count == maksimum)
            {
                result += intArray[intArray.Length - 1].ToString() + " ";
            }

            Console.WriteLine(result);

        }
    }
}
