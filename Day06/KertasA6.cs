﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class KertasA6
    {
        public static void Resolve()
        {
            Console.WriteLine("Cetak kertas A: ");
            string cetak = Console.ReadLine().ToUpper();

            string[] cetakArray = new string [] {"A6", "A5", "A4", "A3", "A2", "A1", "A0" };

            int banyakCetakan = 0;

            for (int i = 0; i < cetakArray.Length; i++)
            {
                if (cetak == cetakArray[i])
                {
                    banyakCetakan = Convert.ToInt32(Math.Pow(2,i));
                }
            }
            Console.WriteLine("banyakCetakan yang dibutuhkan " + banyakCetakan);

        }

    }
}
