﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Kertas A6");
                        Console.WriteLine();
                        KertasA6.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("02. Urutan Abjad");
                        Console.WriteLine();
                        UrutanAbjad.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("03. Tarif Parkir");
                        Console.WriteLine();
                        TarifParkir.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("04. Belanja Online");
                        Console.WriteLine();
                        BelanjaOnline.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("05.Gambreng");
                        Console.WriteLine();
                        Gambreng.Resolve();
                        break;

                    //case 6:

                    //    Console.WriteLine("06.Kacamata Dan Baju");
                    //    Console.WriteLine();
                    //    KacamataDanBaju.Resolve();
                    //    break;

                    //case 7:
                    //    Console.WriteLine("07. LilinFibonacci");
                    //    Console.WriteLine();
                    //    LilinFibonacci.Resolve();
                    //    break;

                    //case 8:
                    //    Console.WriteLine("08. Int Geser");
                    //    Console.WriteLine();
                    //    IntGeser.Resolve();
                    //    break;

                    //case 9:
                    //    Console.WriteLine("09. Parkir");
                    //    Console.WriteLine();
                    //    Parkir.Resolve();
                    //    break;

                    //case 10:
                    //    Console.WriteLine("10. Naik Gunung");
                    //    Console.WriteLine();
                    //    NaikGunung.Resolve();
                    //    break;

                    //case 11:
                    //    Console.WriteLine("11. Kaos Kaki");
                    //    Console.WriteLine();
                    //    KaosKaki.Resolve();
                    //    break;

                    //case 12:
                    //    Console.WriteLine("12. Pembulatan");
                    //    Console.WriteLine();
                    //    Pembulatan.Resolve();
                    //    break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
