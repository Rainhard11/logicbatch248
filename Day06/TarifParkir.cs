﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class TarifParkir
    {
        public static void Resolve()
        {
            Console.WriteLine("Tanggal dan Waktu Masuk: ");
            string waktuMasuk = Console.ReadLine();

            Console.WriteLine("Tanggal dan Waktu Keluar: ");
            string waktuKeluar = Console.ReadLine();

            DateTime masuk = Convert.ToDateTime(waktuMasuk);
            DateTime keluar = Convert.ToDateTime(waktuKeluar);

            double selisihWaktu = (keluar - masuk).TotalDays;

            double lamaParkir = selisihWaktu * (double)24;

            double biaya = 0;

            if (lamaParkir <= 8)
            {
                biaya = lamaParkir * 1000;
                biaya = biaya - (biaya % 1000);
            }
            else if (lamaParkir > 8 && lamaParkir < 24)
            {
                biaya = biaya - (biaya % 1000) + 8000;
            }
            else if (lamaParkir > 24)
            {
                double temp = lamaParkir;
                int hari = 0;
                while (temp > 24)
                {
                    temp = Convert.ToInt32(temp) - 24;
                    hari++;
                }

                int parkir24 = 15000 * hari;
                double siswaWaktu = lamaParkir - (hari * 24) ;

                if (siswaWaktu <= 8)
                {
                    biaya = siswaWaktu * 1000;
                    biaya = biaya - (biaya % 1000) + parkir24;
                }
                else if (siswaWaktu > 8 && siswaWaktu < 24)
                {
                    biaya = biaya - (biaya % 1000) + 8000 + parkir24;
                }

            }

            Console.WriteLine("Biaya Parkir anda adalah : Rp." + biaya);

        }
    }
}
