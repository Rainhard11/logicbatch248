﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Bensin
    {
        public static void Resolve()
        {
            Console.WriteLine("Perjalanan ke");
            string perjalanan = Console.ReadLine().ToLower();

            string[] splitJalan = perjalanan.Split('-');
            int jarak = 0;

            for (int i = 0; i < splitJalan.Length; i++)
            {
                if (splitJalan[i] == "toko")
                {
                    if (jarak > 0)
                    {
                        if (splitJalan[i - 1] == "tempat4")
                        {
                            jarak = jarak + 6500;

                        }
                        else if (splitJalan[i - 1] == "tempat3")
                        {
                            jarak = jarak + 4000;

                        }
                        else if (splitJalan[i - 1] == "tempat2")
                        {
                            jarak = jarak + 2500;

                        }
                        else if (splitJalan[i - 1] == "tempat1")
                        {
                            jarak = jarak + 2000;

                        }
                        else if (splitJalan[i - 1] == "toko")
                        {
                            jarak = jarak + 0;

                        }
                    }
                }
                else if (splitJalan[i] == "tempat1")
                {

                    if (splitJalan[i - 1] == "tempat4")
                    {
                        jarak = jarak + 4500;

                    }
                    else if (splitJalan[i - 1] == "tempat3")
                    {
                        jarak = jarak + 2000;

                    }
                    else if (splitJalan[i - 1] == "tempat2")
                    {
                        jarak = jarak + 500;

                    }
                    else if (splitJalan[i - 1] == "toko")
                    {
                        jarak = jarak + 2000;
                    }


                }
                else if (splitJalan[i] == "tempat2")
                {
                    if (splitJalan[i - 1] == "tempat4")
                    {
                        jarak = jarak + 4000;

                    }
                    else if (splitJalan[i - 1] == "tempat3")
                    {
                        jarak = jarak + 2000;

                    }
                    else if (splitJalan[i - 1] == "tempat1")
                    {
                        jarak = jarak + 500;
                    }
                    else if (splitJalan[i - 1] == "toko")
                    {
                        jarak = jarak + 2500;
                    }

                }
                else if (splitJalan[i] == "tempat3")
                {
                    if (splitJalan[i - 1] == "tempat4")
                    {
                        jarak = jarak + 2500;

                    }
                    else if (splitJalan[i - 1] == "tempat2")
                    {
                        jarak = jarak + 1500;

                    }
                    else if (splitJalan[i - 1] == "tempat1")
                    {
                        jarak = jarak + 2000;
                    }
                    else if (splitJalan[i - 1] == "toko")
                    {
                        jarak = jarak + 4000;
                    }
                }

                else if (splitJalan[i] == "tempat4")
                {
                    if (splitJalan[i - 1] == "tempat3")
                    {
                        jarak = jarak + 2500;

                    }
                    else if (splitJalan[i - 1] == "tempat2")
                    {
                        jarak = jarak + 4000;

                    }
                    else if (splitJalan[i - 1] == "tempat1")
                    {
                        jarak = jarak + 4500;

                    }
                    else if (splitJalan[i - 1] == "toko")
                    {
                        jarak = jarak + 6500;
                    }
                }
            }

            double temp = Convert.ToDouble(jarak) / 1000;
            double bensin = 0;
            while (temp > 0)
            {
                if (temp >= 2.5)
                {
                    temp = temp - 2.5;
                    bensin++;
                }

                else if (temp < 2.5)
                {
                    bensin += temp * 0.4;
                    temp = 0;
                }
            }
            Console.WriteLine(bensin + " Liter Bensin");

        }
    }
}
