﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class KonversiVolume
    {
        public static void Resolve()
        {
            Console.WriteLine("masukan inputan: ");
            string inputan = Console.ReadLine();
            string[] SplitInputan = inputan.Split(' ');

            int jumlahInputan = Convert.ToInt32(SplitInputan[0]);
            string namaInputan = SplitInputan[1];


            Console.WriteLine("dikonversi ke: ");
            string konversi = Console.ReadLine().ToLower(); ;

            double temp = 0;
            if (namaInputan == "cangkir")
            {
                if (konversi == "gelas")
                {
                    temp = jumlahInputan / 2.5;
                }
                else if (konversi == "botol")
                {
                    temp = jumlahInputan / 5;
                }
                else if (konversi == "teko")
                {
                    temp = jumlahInputan / 25;
                }
            }

            else if (namaInputan == "gelas")
            {
                if (konversi == "cangkir")
                {
                    temp = jumlahInputan * 2.5;
                }
                else if (konversi == "botol")
                {
                    temp = jumlahInputan / 2;
                }
                else if (konversi == "teko")
                {
                    temp = jumlahInputan / 10;
                }
            }

            else if (namaInputan == "botol")
            {
                if (konversi == "cangkir")
                {
                    temp = jumlahInputan * 5;
                }
                else if (konversi == "gelas")
                {
                    temp = jumlahInputan * 2;
                }
                else if (konversi == "teko")
                {
                    temp = jumlahInputan / 5;
                }
            }

            else if (namaInputan == "teko")
            {
                if (konversi == "cangkir")
                {
                    temp = jumlahInputan * 25;
                }
                else if (konversi == "gelas")
                {
                    temp = jumlahInputan * 10;
                }
                else if (konversi == "botol")
                {
                    temp = jumlahInputan * 5;
                }
            }

            Console.WriteLine(jumlahInputan + " " + namaInputan + " = " + temp + " " + konversi);


        }
    }
}
