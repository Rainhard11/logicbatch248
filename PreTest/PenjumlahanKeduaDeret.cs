﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class PenjumlahanKeduaDeret
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan panjang n");
            int length = int.Parse(Console.ReadLine());

            int bilanganPrima = 2;
            int panjangBilanganPrima = 0;

            int[] primaArray = new int[length];
            int[] fibonaciArray = new int[length];

            int indexBantu = 0;
            while (panjangBilanganPrima < length)
            {
                int faktorBilanganPrima = 0;

                for (int i = 1; i <= bilanganPrima; i++)
                {
                    if (bilanganPrima % i == 0)
                    {
                        faktorBilanganPrima += 1;
                    }
                }

                if (faktorBilanganPrima == 2) 
                {
                    primaArray[indexBantu]= bilanganPrima;
                    panjangBilanganPrima++;
                    indexBantu++;
                }
                bilanganPrima++;
            }

            int bilangan1 = 1;
            int bilangan2 = 0;
            int hasil = 0;

            for (int i = 0; i < length; i++)
            {
                hasil = bilangan1 + bilangan2;
                fibonaciArray[i] = hasil;
                bilangan1 = bilangan2;
                bilangan2 = hasil;
            }

            for (int i = 0; i < length; i++)
            {
                Console.Write((fibonaciArray[i] + primaArray[i]) + " ");
            }
        }
    }
}
